const sideBarReducer = (state=true,action) =>{
    switch (action.type) {
        case "SIDE_BAR":
            return !state;
        default: return state;
    }
}

export default sideBarReducer;
