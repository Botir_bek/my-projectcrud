import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';
import DirectionsBoatIcon from '@material-ui/icons/DirectionsBoat';
import {Link} from "react-router-dom";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Badge from "@material-ui/core/Badge";
import MailIcon from '@material-ui/icons/Mail';
import {useDispatch, useSelector} from "react-redux";
import MenuIcon from '@material-ui/icons/Menu';
import {sideBar,signOut} from "../store/actions";
import Hidden from "@material-ui/core/Hidden";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';


const useStyles = makeStyles((theme) => ({
    root: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        display:"flex",

    },
    convert:{
        color: "#ffffff !inherit",
        marginLeft: 10,

    }
}));


function ScrollTop(props) {
    const { children, window } = props;
    const classes = useStyles();

    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');

        if (anchor) {
            anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    };

    return (
        <Zoom in={trigger}>
            <div onClick={handleClick} role="presentation" className={classes.root}>
                {children}
            </div>
        </Zoom>
    );
}

ScrollTop.propTypes = {
    children: PropTypes.element.isRequired,
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};


export default function Header(props) {

    const classes = useStyles();
    const counter = useSelector(state => state.counter);
    const dispatch = useDispatch();

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar style={{background:"#192a56"}}>
                <Toolbar>
                    <Link to="/">
                        <IconButton
                            edge="start"
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="menu">
                            <p style={{fontSize:28,textMargin:"center",marginBottom: 0}}>🧐</p>
                            {/*<DirectionsBoatIcon style={{fontSize:30,color:"#fff"}}/>*/}
                        </IconButton>
                    </Link>

                    <div className={classes.title}>
                        <Typography variant="h6" >
                            Welcome
                        </Typography>

                    </div>
                    <Badge
                        badgeContent={counter}
                        color="secondary"
                        style={{marginRight:10}} >
                        <MailIcon  />
                    </Badge>

                        <IconButton onClick={()=>{dispatch(signOut())}}>
                            <ExitToAppIcon style={{color:"#fff"}}/>
                        </IconButton>

                    {/*for hidden size !!only={[ 'lg']}*/}
                    <Hidden >
                        <IconButton onClick={()=>{dispatch(sideBar())}}>
                            <MenuIcon style={{color:"#fff"}}/>
                        </IconButton>
                    </Hidden>


                </Toolbar>
            </AppBar>
            {/*<AppBar >*/}
            {/*    <Toolbar>*/}
            {/*        /!*<Typography variant="h6" component={Link} to="/">Contanct lists</Typography>*!/*/}
            {/*        <Link to="/">*/}
            {/*        <IconButton  className={classes.menuButton} style={{color:"#c0e0c5"}} component={Link} to="/" aria-label="menu" >*/}
            {/*            <HomeIcon style={{fontSize:30}}/>*/}
            {/*        </IconButton>*/}

            {/*        </Link>*/}
            {/*        <Link to="/auth"> <Button style={{color:"#fff"}}>Logout</Button></Link>*/}
            {/*    </Toolbar>*/}
            {/*</AppBar>*/}
            <Toolbar id="back-to-top-anchor" />

            <ScrollTop {...props}>
                <Fab color="secondary" size="small" aria-label="scroll back to top">
                    <KeyboardArrowUpIcon />
                </Fab>
            </ScrollTop>
        </React.Fragment>
    );
}

