import React from "react";
import styled from "styled-components";

const FooterMain = styled.div`
    width:100%;
    background:#c0d8ed;
    margin-top: 10px;
    
`;

const FotTitle = styled.h4`
    margin: 0 auto;
    padding-top:20px;
    text-align: center;
    margin-bottom:5px;
`;
const Text = styled.p`
     margin: 0 auto;
     text-align: center;
     padding-bottom:20px;

`

export default function Footer() {
    return(
      <FooterMain>
          <FotTitle>Info section</FotTitle>
          <Text>©Tashkent 2021  <span style={{marginLeft:30}}></span>email: bek.info@gmail.com</Text>
      </FooterMain>
    );

}
