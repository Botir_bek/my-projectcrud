import React, {useEffect, useState} from 'react';
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import DoConvert from "../Functions/KirilLotin"
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Button from "@material-ui/core/Button";
const arr=[];
export default function Convertor() {
    const [text, setText]=useState({type:"",lotin:"", kiril:""});
    const [result ,setResult]=useState({res:""});
    const {type,lotin, kiril} = text;
    function HandleChange(e) {

        setText({...text, [e.target.name]: e.target.value})
    }
    function doConvert()
    {
        text.type != "kiril" 
        ? setResult({res:DoConvert(text)})
        : setResult({res:"Yaqin orada ishlab chiqish kutilmoqda..."})

    }
    function deleteTexts() {
        setResult({res:""});
        setText({lotin: ""});
    }
    return(
        <div style={{width:"90%",margin: "auto"}}>
            <Box my={2}>
                <Card style={{padding:20, marginBottom:20,}}>
                    <h1 style={{textAlign:"center",fontFamily:"Arial Rounded MT Bold"}}>{text.type != "kiril" ? "Kiril lotin tarjimon" : "Кирил лотин таржимон"}</h1>
                    <select
                        className="form-control"
                        name="type"
                        onChange={HandleChange}
                        style={{
                            background:"#b6aaaa",
                            width:100
                        }}
                    >
                        <option value="lotin">Lotin</option>
                        <option value="kiril">Кирил</option>
                    </select>
                    <div style={{display:'flex',justifyContent:"space-between",marginTop:20}}>
                        <div className="mb-3 mr-4" style={{flex:1}}>

                            <label htmlFor="exampleFormControlInput1" className="form-label">{text.type != "kiril"?"Lotin":"Кирил"}</label>
                            <textarea
                                className="form-control"
                                id="exampleFormControlTextarea1"
                                rows="14"
                                name="lotin"
                                value={text.lotin}
                                placeholder={`${text.type != "kiril"?"Tekist kiriting:":"Тeкист киритинг:"}`}
                                style={{
                                    background:"#e1e1",
                                    resize:"none"
                                }}
                                onChange={HandleChange}
                            ></textarea>
                        </div>
                        <div className="mb-3" style={{flex:1}}>
                            <label htmlFor="exampleFormControlTextarea1" className="form-label">{text.type != "kiril"?"NATIJA":"НАТИЖА"}</label>
                            <textarea
                                className="form-control"
                                id="exampleFormControlTextarea1"
                                rows="14"
                                name="result"
                                value={result.res}
                                placeholder={`${text.type != "kiril"?"Кутилаётган натижа:":"Kutilayotgan natija:"}`}
                                style={{
                                    background:"#e1e1",
                                    resize:"none"
                                }}
                                onChange={HandleChange}
                            ></textarea>
                        </div>
                    </div>
                    <div style={{display:"flex",justifyContent:"space-between"}}>
                        <button
                            className="btn btn-primary btn-block"
                            style={{width:200,}}
                            onClick={doConvert}
                        >{text.type !== "kiril"? "O`girish":"Ўгириш"}</button>
                        <Button variant="outlined" color="secondary" onClick={deleteTexts} style={{}}>
                            <DeleteForeverIcon/>
                        </Button>
                    </div>

                </Card>
            </Box>
        </div>
    );

}
