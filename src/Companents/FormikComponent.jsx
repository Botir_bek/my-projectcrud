import React, {useState} from 'react';
import { useFormik } from 'formik';
import {useDispatch, useSelector} from "react-redux";
import {increment, decrement, getObjekt,lists} from "../store/actions";

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'Required';
    } else if (values.firstName.length > 15) {
        errors.firstName = 'Must be 15 characters or less';
    }

    if (!values.lastName) {
        errors.lastName = 'Required';
    } else if (values.lastName.length > 20) {
        errors.lastName = 'Must be 20 characters or less';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }

    return errors;
};
const FormikComponennt = () => {
    const dispatch =useDispatch();
    const getObj = useSelector(state =>state.getObj)
    const data = useSelector(state => state.list)
    const [item, setItem] =useState({firstName:"", lastName:"",email:""});
    const [listName,setListName] = useState({listName:""})
    // Notice that we have to initialize ALL of fields with values. These
    // could come from props, but since we don't want to prefill this form,
    // we just use an empty string. If you don't do this, React will yell
    // at you.

    const addList = () =>{
       // dispatch(lists(listName.listName))
    }
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
        },
        validate,
        onSubmit: values => {
            // alert(JSON.stringify(values, null, 2));
            // console.log(values);
            setItem(values);
            dispatch(getObjekt(values));
            dispatch(lists(values));
        },
    });
    return (
        <div className="card-body" style={{margin:"auto"}}>
        <form onSubmit={formik.handleSubmit} >
            <label className="col-form-label" htmlFor="firstName">First Name</label>
            <div className="form-group">
                <input
                    className="form-control form-control-lg w-25 h-25"
                    id="firstName"
                    name="firstName"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.firstName}
                />
                {formik.errors.firstName ? <div className="text-danger">{formik.errors.firstName}</div> : null}

            </div>

            <label htmlFor="lastName">Last Name</label>
            <div className="form-group">
                <input
                    className="form-control form-control-lg w-25 h-25"
                    id="lastName"
                    name="lastName"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.lastName}
            />
                {formik.errors.lastName ? <div className="text-danger">{formik.errors.lastName}</div> : null}
            </div>
            <label htmlFor="email">Email Address</label>
            <div className="form-group">
                <input
                    className="form-control form-control-lg w-25 h-25"
                    id="email"
                    name="email"
                    type="email"
                    onChange={formik.handleChange}
                    value={formik.values.email}
            />
                {formik.errors.email ? <div className="text-danger">{formik.errors.email}</div> : null}

            </div>
            <button
                className="btn btn-primary"
                type="submit"
                disabled={
                    formik.errors.email
                    ||formik.errors.lastName
                    ||formik.errors.firstName
                    ?true:false}
            >Submit</button>
        </form>
            {item.firstName.length > 0 &&
            <div className="mt-5">
                <div className="alert alert-primary w-25 " role="alert">
                {item.firstName}
                </div>
                <div className="alert alert-danger w-25" role="alert">
                {item.lastName}
                </div>
                <div className="alert alert-dark w-25" role="alert">
                {item.email}
                </div>
            </div>
            }
            <h3>{ getObj.firstName} {getObj.lastName} {getObj.email}</h3>
            {/*<button className="btn btn-primary mt-5" onClick={()=>dispatch(getObj(item))}>getObj</button>*/}
            <button className="btn btn-primary mt-5" onClick={()=>dispatch(increment(1  ))}>counter</button>
            <button className="btn btn-danger  ml-1 mt-5" onClick={()=>dispatch(decrement())}>decrement</button>

            <ul>
                {data.map((item, key)=>{
                    return(
                        <li key={key}>{item.lastName}</li>
                    )
                })}
            </ul>
        </div>
    );
};
export default FormikComponennt;
