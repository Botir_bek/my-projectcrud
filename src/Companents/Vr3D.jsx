import React from 'react';
import minor from '../assets/6.jpg';
import beach from '../assets/beach.jpg'
import panorama from '../assets/5.jpg';
import '../assets/styles/vr.css'
export function Vr3D() {
    //
    return(
       <div >
           <h1>360 degree pictures</h1>

           <a-scene style={{zIndex:0}}>
            <img id="panorama"  src={panorama}/>
            <a-sky src="#panorama" rotation="0 -90 0" ></a-sky>
           </a-scene>

           {/*<div id="pano"></div>*/}
       </div>
    )

}
