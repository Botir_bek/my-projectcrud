import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        width:400,
        marginTop: 30,
        marginLeft: 30,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9

    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

const defURL="https://www.kmo.com.au/images/user-persona-in-web-design-and-user-expereince-.jpg";
export default function Users(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const user = props.user;
    const handleExpandClick = () => {
        setExpanded(!expanded);

    };

    return (


        <Card className={classes.root}>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>

                    </Avatar>
                }
                action={
                    <IconButton aria-label="settings">
                        <MoreVertIcon />
                    </IconButton>
                }
                title={user.name}
                subheader={user.email}
            />
            <CardMedia
                className={classes.media}
                image={user.image}
                title={user.name}
                style={{height:100}}
            />
            <CardContent>

                <Typography variant="body2" color="textSecondary" component="p">
                        {user.message}
                    Tel: {user.contact}

                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton component={Link} to="/example" aria-label="add to favorites">
                    <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                    <ShareIcon />
                </IconButton>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                {/*//any coment or description*/}
            </Collapse>
        </Card>

    );
}
