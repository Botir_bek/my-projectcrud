import React from "react";
import styled from 'styled-components';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Tooltip from "@material-ui/core/Tooltip";
import makeStyles from "@material-ui/core/styles/makeStyles";
import ForumTwoToneIcon from '@material-ui/icons/ForumTwoTone';
import DnsTwoToneIcon from '@material-ui/icons/DnsTwoTone';
import LibraryAddTwoToneIcon from '@material-ui/icons/LibraryAddTwoTone';
import CollectionsBookmarkTwoToneIcon from '@material-ui/icons/CollectionsBookmarkTwoTone';
import PermContactCalendarTwoToneIcon from '@material-ui/icons/PermContactCalendarTwoTone';
import {Link} from "react-router-dom";


const Main = styled.div`
        
    height:100%;
    width:65px;
    position:fixed;
    background: #eee;
    transition: all 3s easy;
    
`
const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
        color: theme.palette.common.black,
        marginLeft:-5,
    },
    tooltip: {
        backgroundColor: theme.palette.common.black,
        height: "30px",
        textAlign: "center",
        fontSize:12,
        lineHeight:2,
    },
}));

const sideBar = [
    {
        text: 'List',
        icon: <DnsTwoToneIcon style={{fontSize:33,color:"rgb(44 169 100)"}}/>,
        link: '/'
    },
    {
        text: "Users",
        icon: <PermContactCalendarTwoToneIcon style={{fontSize:33,color:"rgb(45 57 50)"}}/>,
        link: "/users"
    },

    {
        text: 'Convertor',
        icon: <ForumTwoToneIcon style={{fontSize:33,color:"rgb(45 57 50)"}}/>,
        link: "/convert"
    },
    {
        text: 'AddUser',
        icon: <LibraryAddTwoToneIcon style={{fontSize:33,color:"rgb(45 57 50)"}}/>,
        link:"/add"
    },
    {
        text: 'Formik',
        icon: <CollectionsBookmarkTwoToneIcon style={{fontSize:33,color:"rgb(45 57 50)"}}/>,
        link: "/formik"
    },
    {
        text: 'VR',
        icon: <CollectionsBookmarkTwoToneIcon style={{fontSize:33,color:"rgb(45 57 50)"}}/>,
        link: "/vr"
    },


];
function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();

    return <Tooltip arrow classes={classes} {...props} />;
}
export default function SideBar(){
    return(
        <div style={{transition:"all 3s easy-out",zIndex:999}}>
            <Main  className="main" styed={{}}>
                <List>
                    {sideBar.map((item, index) => (
                        <BootstrapTooltip title={item.text} placement="right">
                            <ListItem button key={item.text}>
                                <Link to={item.link}>
                                    <ListItemIcon style={{minWidth:"0, !inherit"}} >{item.icon}</ListItemIcon>
                                    {/*<ListItemText primary={text} style={{marginRight:20}} />*/}
                                </Link>
                            </ListItem>
                        </BootstrapTooltip>
                    ))}

                </List>
            </Main>
        </div>
    );
}
