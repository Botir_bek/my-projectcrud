import React, {useState,useEffect} from 'react';
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import {Link} from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import axios from 'axios';
import {useHistory, useParams} from 'react-router-dom';


export default function EditForm(){
        let history = useHistory();
        const {id} = useParams();
        console.log('id prop--->',id)
        const [user, setUser]=useState({
            name: "",
            contact: "",
            email: "",
            message: "",
            image: ""
        })
        const {name,contact,email,message,image} = user;
        const onInputChange = e=>{
            console.log(e.target.value)

            setUser({...user, [e.target.name]: e.target.value});
        }

        useEffect(() => {
            loadUser();
            console.log(user);
        },[])
        const onSubmit = async e => {
            e.preventDefault();
            await axios.put(`http://localhost:3000/users/${id}`,user);
            history.push("/")
        }
        const loadUser = async ()=>{
            const result = await axios.get(`http://localhost:3000/users/${id}`);
            setUser(result.data);
            console.log("----response-----")
            console.log(result.data);
        }

        return(
            <div>
                <Container maxWidth="sm">
                    <Box my={2}>
                        <Card style={{padding:20, marginBottom:20,}}>
                            <h1 style={{marginLeft:100}} >Edit information</h1>

                            <form onSubmit={e=>onSubmit(e)}>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-lg"
                                        //placeholder={user.name}
                                        name="name"
                                        value={user.name}
                                        onChange={e => onInputChange(e)}
                                    />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-lg"
                                        //placeholder={user.contact}
                                        name="contact"
                                        value={user.contact}
                                        onChange={e => onInputChange(e)}
                                    />
                                </div><div className="form-group">
                                <input
                                    type="text"
                                    className="form-control form-control-lg"
                                    //placeholder={user.email}
                                    name="email"
                                    value={user.email}
                                    onChange={e => onInputChange(e)}
                                />
                            </div>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-lg"
                                        //placeholder={user.message}
                                        name="message"
                                        value={user.message}
                                        onChange={e => onInputChange(e)}
                                    />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-lg"
                                        //placeholder={user.message}
                                        name="image"
                                        value={user.image}
                                        onChange={e => onInputChange(e)}
                                    />
                                </div>
                                <button className="btn btn-warning btn-block w-50 ">Update information</button>

                            </form>

                        </Card>
                    </Box>
                </Container>
            </div>
        )

}
