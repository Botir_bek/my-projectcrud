import React, {useState} from 'react';
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import {Link} from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import axios from 'axios';
import {useHistory} from 'react-router-dom';


export default function AddForm(){
    let history =useHistory();
    const [user, setUser]=useState({
        name: "",
        contact: "",
        email: "",
        message: "",
        image:""
    })
    const {name,contact,email,message,image} = user;
    const onInputChange = e=>{
        console.log(e.target.value)
        setUser({...user, [e.target.name]: e.target.value});
    }

    const onSubmit = async e => {
        e.preventDefault();
        await axios.post("http://localhost:3000/users",user);
        history.push("/")
    }
    return(
        <div>
            <Container style={{maxWidth:500}}>
                <Box my={2}>
                    <Card style={{padding:20, marginBottom:20,}}>
                        <h1>Additional member</h1>

                        <form onSubmit={e=>onSubmit(e)} >

                            <div className="form-group">
                                <TextField
                                    id="filled-basic"
                                    label="Full Name"
                                    style={{width:"100%"}}
                                    variant="filled"
                                    name="name"
                                    vlue={name}
                                    onChange={e => onInputChange(e)}/>

                                </div>
                            <div className="form-group">
                                <TextField
                                    id="filled-basic"
                                    label="Contact"
                                    style={{width:"100%"}}
                                    variant="filled"
                                    name="contact"
                                    vlue={contact}
                                    onChange={e => onInputChange(e)}/>
                            </div>
                            <div className="form-group">
                                <TextField
                                    id="filled-basic"
                                    label="Email"
                                    style={{width:"100%"}}
                                    variant="filled"
                                    name="email"
                                    vlue={email}
                                    onChange={e => onInputChange(e)}/>
                            </div>
                            <div className="form-group">
                                <TextField
                                    id="filled-basic"
                                    label="Message"
                                    style={{width:"100%"}}
                                    variant="filled"
                                    name="message"
                                    vlue={message}
                                    onChange={e => onInputChange(e)}
                                />
                            </div>
                            <div className="form-group">
                                <TextField
                                    id="filled-basic"
                                    label="Image"
                                    style={{width:"100%"}}
                                    variant="filled"
                                    name="image"
                                    vlue={image}
                                    onChange={e => onInputChange(e)}/>
                            </div>
                            <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                            >Add information</Button>

                        </form>
                        {/*<form onSubmit={e=>onSubmit(e)}>*/}
                        {/*<Grid container justify="center"  spacing={3} >*/}

                        {/*  <Grid item lg={12} md={12}>*/}
                        {/*      <TextField*/}
                        {/*          style={{width:400,marginLeft:50}}*/}
                        {/*          id="outlined-search"*/}
                        {/*          label="Enter name"*/}
                        {/*          type="text"*/}
                        {/*          name="name"*/}
                        {/*          value={name}*/}
                        {/*          variant="outlined"*/}
                        {/*          onChange={e => onInputChange(e)}*/}

                        {/*      />*/}
                        {/*  </Grid>*/}
                        {/*  <Grid item lg={12} md={12}>*/}
                        {/*      <TextField*/}
                        {/*          style={{width:400,marginLeft:50}}*/}
                        {/*          id="outlined-search"*/}
                        {/*          label="Enter contact"*/}
                        {/*          type="text"*/}
                        {/*          name="contact"*/}
                        {/*          value={contact}*/}
                        {/*          variant="outlined"*/}
                        {/*          onChange={e => onInputChange(e)}*/}

                        {/*      />*/}
                        {/*  </Grid>*/}
                        {/*  <Grid item lg={12} md={12}>*/}
                        {/*      <TextField*/}
                        {/*          style={{width:400,marginLeft:50}}*/}
                        {/*          id="outlined-search"*/}
                        {/*          label="Enter email"*/}
                        {/*          type="email"*/}
                        {/*          name="email"*/}
                        {/*          value={email}*/}
                        {/*          variant="outlined"*/}
                        {/*          onChange={e => onInputChange(e)}*/}

                        {/*      />*/}
                        {/*  </Grid>*/}
                        {/*  <Grid item lg={12} md={12}>*/}
                        {/*      <TextField*/}
                        {/*          style={{width:400,marginLeft:50}}*/}
                        {/*          id="outlined-search"*/}
                        {/*          label="Enter message"*/}
                        {/*          type="email"*/}
                        {/*          name="message"*/}
                        {/*          value={message}*/}
                        {/*          variant="outlined"*/}
                        {/*          onChange={e => onInputChange(e)}*/}

                        {/*      />*/}
                        {/*  </Grid>*/}
                        {/*  <Grid item justify="center" lg={12} md={12}>*/}
                        {/*      <Button component={Link} to="/edit" onSubmit={e=>onSubmit(e)} variant="contained" color="primary" style={{marginLeft:150}}>*/}
                        {/*          Add information*/}
                        {/*      </Button>*/}
                        {/*  </Grid>*/}
                        {/*</Grid>*/}
                        {/*</form>*/}
                    </Card>
                </Box>
            </Container>
        </div>
    )

}
